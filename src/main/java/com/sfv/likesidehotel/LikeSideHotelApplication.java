package com.sfv.likesidehotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikeSideHotelApplication {

    public static void main(String[] args) {
        SpringApplication.run(LikeSideHotelApplication.class, args);
    }

}
