package com.sfv.likesidehotel.repository;

import com.sfv.likesidehotel.model.BookedRoom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookedRoomRepository extends JpaRepository<BookedRoom,Long> {
    List<BookedRoom> findAllByRoomId(Long id);

    BookedRoom  findByBookingConfirmationCode(String confirmationCode);
}
